#zad 1
import numpy as np

#zad 2
print(np.__version__)
np.show_config()

#zad 3
v1 = np.zeros(10)
print(v1)

#zad 4
print(v1.size)

#zad 5
print(np.info(np.add))

#zad 6
v2 = np.zeros(10)
v2[4] = 1
print(v2)

#zad 7
print(np.info(np.arange))
v3 = np.arange(10, 49+1)
print(v3)

#zad 8
print(v3[::-1])

#zad 9
print(np.info(np.reshape))
v4 = np.arange(0, 8+1)
print(np.reshape(v4, (3, 3)))

#zad 10
v5 = [1, 2, 0, 0, 4, 0]
print(np.nonzero(v5))

#zad 11
print(np.info(np.eye))
print(np.eye(3))

#zad 12
print(np.info(np.random.random))
v6 = np.random.random(3*3*3)
print(np.reshape(v6, (3, 3, 3)))

#zad 13
v7 = np.random.random(10*10).reshape(10, 10)
print(v7)
print(np.min(v7))
print(np.max(v7))

#zad 14
v8 = np.random.random(30)
print(v8)
print(np.mean(v8))

#zad 15
v9 = np.ones(4*4).reshape(4, 4)
v9[1:-1, 1:-1] = 0
print(v9)

#zad 16
print(np.info(np.pad))
v10 = np.arange(0, 8+1)
print(np.pad(v10.reshape(3, 3), 1, 'constant', constant_values=0))

#zad 17
print(0 * np.nan)
print(np.nan == np.nan)
print(np.inf > np.nan)
print(np.nan - np.nan)
print(np.nan in set([np.nan]))
print(0.3 == 3 * 0.1)

#zad 18
